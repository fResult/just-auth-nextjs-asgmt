const withTM = require('next-transpile-modules')([
  '@mui/material',
  '@mui/system',
  '@mui/icons-material', // If @mui/icons-material is being used
])

/** @type {import('next').NextConfig} */
const nextConfig = withTM({
  // ...nextConfig,
  experimental: {
    appDir: true,
  },
  env: {
    API_URL: 'http://tw-mgt-dev.magicboxsolution.com:8080/pub/rest/en/V1',
  },
  webpack: (config) => {
    config.resolve.alias = {
      ...config.resolve.alias,
      '@mui/styled-engine': '@mui/styled-engine-sc',
    }
    config.module = {
      ...config.module,
      rules: [
        {
          test: /\.js|jsx|ts|tsx$/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['next/babel'],
              plugins: [
                [
                  'babel-plugin-styled-components',
                  {
                    topLevelImportPaths: [
                      '@mui/material',
                      '@mui/material/styles',
                      '@mui/system',
                      '@mui/styled-engine-sc',
                      '@mui/styled-engine',
                    ],
                    ssr: true,
                  },
                ],
              ],
              include: [path.resolve('*'), path.resolve('node_modules/@mui/')],
              exclude: /node_modules\/(?!@mui).+/,
            },
          },
        },
      ],
    }
    return config
  },
})

module.exports = nextConfig
