import { AppProps } from 'next/app'

import { ChakraProvider } from '@chakra-ui/react'

import { colors } from '@!/constants/colors'

const MyApp = ({ Component, pageProps }: AppProps) => {
  return (
    <ChakraProvider theme={colors}>
      <Component {...pageProps} />
    </ChakraProvider>
  )
}

export default MyApp
