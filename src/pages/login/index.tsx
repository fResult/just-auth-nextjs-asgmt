import { TextField } from '@mui/material'
import React, { FC } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { IFormLogin } from '@:/forms'

type LoginProps = {
  className?: string
}

const Login: FC<LoginProps> = ({ className = '' }) => {
  const { control, handleSubmit, formState } = useForm<IFormLogin>()

  const handleLogin = handleSubmit(function handleLogin(formValue: IFormLogin) {
    console.log('formValue', formValue)
  })

  return (
    <main className={className}>
      <p>Login</p>
      <form onSubmit={handleLogin}>
        <Controller
          name="email"
          control={control}
          render={({ field }) => (
            <TextField
              label="Email"
              placeholder="email@example.com"
              variant="outlined"
              {...field}
            />
          )}
        />
        <Controller
          name="password"
          control={control}
          render={({ field }) => (
            <TextField label="Password" type="password" {...field} />
          )}
        />
        <button type="submit">Login</button>
      </form>
    </main>
  )
}

export default Login
