export const naturalColors = {
  yaleBlue: '#0E3F87',
  glow: '#FF682B',
  basicBlack: '#000000',
  darkHead: '#23272E',
  darkGlow: '#414C5C',
  morningGray: '#818D99',
  heavyGray: '#999FAA',
  basicGray: '#B0B4BB',
  frostGray: '#C4C7CC',
  iceGray: '#DCDEE0',
  whiteGray: '#F3F4F5',
  whiteWool: '#FAFAFA',
  basicWhite: '#FFFFFF',
} satisfies Record<string | number, string>

export const stateColors = {
  noticeRed: '#E6656A',
  hoverBlue: '#104799',
  pressBlue: '#0C3573',
  linkBlue: '#394EA2',
} satisfies Record<string | number, string>

export const colors = {
  ...naturalColors,
  ...stateColors
} satisfies Record<string | number, string>
